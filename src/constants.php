
<?php

define("dateFormat", "Y-m-d H:i");
define("minHighTideCoeff", 65);
define("minDelayBeforeSunsetInMinutes", 60);
define("dayIdx", 0);
define("dateIdx", 1);
define("sunriseIdx", 2);
define("sunsetIdx", 3);
define("morningHighTideIdx", 4);
define("morningHighTideCoeffIdx", 6);
define("eveningHighTideIdx", 7);
define("eveningHighTideCoeffIdx", 9);

?>