
<?php

function convertFrenchDateToInternationalDay($frenchDate) {
    $frenchDay = explode(",", $frenchDate)[0];
    $numDay = str_replace("avril", "04", $frenchDay);
    $date = date_create_from_format("j m Y", $numDay);
    return date_format($date,"Y-m-d");
  }

  // https://www.sunrise-and-sunset.com/fr/sun/france/arcachon/2023/avril
function convertSunriseAndSunsetMonthlyData($sourceFileName, $destFileName) {

    if (($monthSun = fopen($sourceFileName, "r")) !== FALSE) {
        if(($monthLight = fopen($destFileName, "w")) !== FALSE) {
            $titles = fgetcsv($monthSun, 0, "\t");
            fwrite($monthLight, "Jour\t" . $titles[0] . "\t" . $titles[1] . "\t" . $titles[2]);
            fwrite($monthLight, "\n");

            while (($daySun = fgetcsv($monthSun, 0, "\t")) !== FALSE) {
                fwrite($monthLight, explode(",", $daySun[0])[1]. "\t" . convertFrenchDateToInternationalDay($daySun[0]) . "\t" . $daySun[1] . "\t" . $daySun[3]);
                fwrite($monthLight, "\n");
            }
            fclose($monthLight);

        }
        fclose($monthSun);
    }
}

convertSunriseAndSunsetMonthlyData("source-soleil-avril.txt", "jour-avril.csv");
?>