
<?php

function writeHtmlFile($ephemerideFileName, $htmlFileName) {

    $htmlTemplate = "
<html>
    <head>
     <link rel=\"stylesheet\" href=\"style.css\"> 

    </head>
    <body>
    {{content}}
    </body>
</html>
    ";

    $html = str_replace("{{content}}", buildMonthTable($ephemerideFileName), $htmlTemplate);

    file_put_contents($htmlFileName, $html);
}


?>
