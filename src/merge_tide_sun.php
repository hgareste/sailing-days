
<?php

function convertFrenchDateToInternationalDay($frenchDate) {
    $frenchDay = explod8e(",", $frenchDate)[0];
    $numDay = str_replace("avril", "04", $frenchDay);
    $date = date_create_from_format("j m Y", $numDay);
    return date_format($date,"Y-m-d");
  }

function buildLine($tideData, $sunData) {
    $line = $sunData[0] . "\t" . $tideData[0];
    $line .= "\t" . $sunData[2] . "\t" . $sunData[3];
    for ($x = 1; $x <= 10; $x++) {
        $line .= "\t" . $tideData[$x];
    }
    return $line;
}

function mergeTideAndSunMonthlyData($tideFileName, $sunFileName, $destFileName) {

    if (($monthTide = fopen($tideFileName, "r")) !== FALSE) {
        if (($monthSun = fopen($sunFileName, "r")) !== FALSE) {
            if(($monthOutput = fopen($destFileName, "w")) !== FALSE) {
                $tideTitles = fgetcsv($monthTide, 0, "\t");
                $sunTitles = fgetcsv($monthSun, 0, "\t");
                fwrite($monthOutput, buildLine($tideTitles, $sunTitles));
                fwrite($monthOutput, "\n");

                while (($dayTide = fgetcsv($monthTide, 0, "\t")) !== FALSE) {
                    if(($daySun = fgetcsv($monthSun, 0, "\t")) !== FALSE) {
                        fwrite($monthOutput, buildLine($dayTide, $daySun));
                        fwrite($monthOutput, "\n");
                    }
                }
                fclose($monthOutput);

            }
            fclose($monthSun);
        }
        fclose($monthTide);
    }
}

mergeTideAndSunMonthlyData("data/source-maree-2023-05.csv", "data/source-soleil-2023-05.csv", "data/ephemeride-2023-05.csv");
?>