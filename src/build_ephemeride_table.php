
<?php

require __DIR__ . '/constants.php';

function minutesBetweenTimes($date, $firstTime, $secondTime) {

    $firstDate = date_create_from_format(dateFormat, $date . " " . $firstTime);
    $secondDate = date_create_from_format(dateFormat, $date . " " . $secondTime);
    if($firstDate == false || $secondDate == false) {
        return -720;
    } else {
        $delayBetweenDates = date_diff($secondDate, $firstDate);
        return  $delayBetweenDates->format("%r%h")*60 + $delayBetweenDates->format("%r%i");
    }
}

function buildMonthTable($monthFileName) {
    if (($monthTide = fopen($monthFileName, "r")) !== FALSE) {

	    $mobileTable = "<div class=\"xs-main\">";
        $table = "<table>\n\t<thead>\n\t\t<tr>";
        $titles = fgetcsv($monthTide, 0, "\t");

        for ($x = 0 ; $x <=eveningHighTideCoeffIdx ; $x++) {
            $table .= "<th>" . $titles[$x] . "</th>";
        }
        $table .= "</tr>\n\t</thead>\n\t<tbody>";

        while (($dayTide = fgetcsv($monthTide, 1000, "\t")) !== FALSE) {

            $className = "";
            $morningClassName = "";
            $eveningClassName = "";
            // coeff > 60 et pleine mer 2h après le lever ou 2h avant le coucher

            $sunrise = date_create_from_format(dateFormat, $dayTide[dateIdx] . " " . $dayTide[sunriseIdx]);
            $morningHighTide = date_create_from_format(dateFormat, $dayTide[dateIdx] . " " . $dayTide[morningHighTideIdx]);

            $sunset = date_create_from_format(dateFormat, $dayTide[dateIdx] . " " . $dayTide[sunsetIdx]);
            $eveningHighTide = date_create_from_format(dateFormat, $dayTide[dateIdx] . " " . $dayTide[eveningHighTideIdx]);

            $minutesBetweenMorningHighTideAndSunrise = minutesBetweenTimes($dayTide[dateIdx], $dayTide[morningHighTideIdx], $dayTide[sunriseIdx]); // must be positive to navigate
            $minutesBetweenSunsetAndEveningHighTide = minutesBetweenTimes($dayTide[dateIdx], $dayTide[sunsetIdx], $dayTide[eveningHighTideIdx]); // must be positive to navigate


            // echo "sunriseStr : ", $sunriseStr, ", sunrise : ", date_format($sunrise,"Y/m/d H:i:s"), "\n";
            // echo "sunrise : ", date_format($sunrise,"Y/m/d H:i:s"), ", sunset : ", date_format($sunset,"Y/m/d H:i:s"), ", morningHighTide : ", date_format($morningHighTide,"Y/m/d H:i:s"), ", eveningHighTide : ", date_format($eveningHighTide,"Y/m/d H:i:s"), "\n";
            // echo "sunrise : ", date_format($sunrise,"Y/m/d H:i:s"), ", morningHighTide : ", date_format($morningHighTide,"Y/m/d H:i:s"), ", diff between : ", $minutesBetweenSunriseAndMorningHighTide , "\n";
            // echo "sunset : ", date_format($sunset,"Y/m/d H:i:s"), ", eveningHighTide : ", date_format($eveningHighTide,"Y/m/d H:i:s"), ", diff between : ", $minutesBetweenSunsetAndEveningHighTide , "\n";

            // echo "day : ", $dayTide[dateIdx], ", morning coeff : ", $dayTide[morningHighTideCoeffIdx], ", delay between morning tide and sunrise : ", $minutesBetweenMorningHighTideAndSunrise, "\n";
            if($dayTide[morningHighTideCoeffIdx] > minHighTideCoeff && $minutesBetweenMorningHighTideAndSunrise > minDelayBeforeSunsetInMinutes) {
                $morningClassName = "sailable";
            }
            if($dayTide[eveningHighTideCoeffIdx] > minHighTideCoeff && $minutesBetweenSunsetAndEveningHighTide > minDelayBeforeSunsetInMinutes) {
                $eveningClassName = "sailable";
            }
            $table .= "\n\t\t<tr>";
            $table .= "<td class=\"" . $morningClassName . " " . $eveningClassName . "\">" . $dayTide[dayIdx] . "</td>";
            $table .= "<td class=\"" . $morningClassName . " " . $eveningClassName . "\">" . $dayTide[dateIdx] . "</td><td>" . $dayTide[sunriseIdx] . "</td><td>" . $dayTide[sunsetIdx] . "</td>";
            for ($x = morningHighTideIdx; $x <= morningHighTideCoeffIdx; $x++) {
                $table .= "<td class=\"" . $morningClassName . "\">" . $dayTide[$x] . "</td>";
            }
        
            for ($x = eveningHighTideIdx; $x <= eveningHighTideCoeffIdx; $x++) {
                $table .= "<td class=\"" . $eveningClassName . "\">" . $dayTide[$x] . "</td>";
            }
        
            $table .= "</tr>";

            if(strlen($morningClassName) > 0 || strlen($eveningClassName) > 0) {
            	$mobileTable .= "\n\t<div class=\"xs-header day\">" . $titles[dayIdx] . "</div><div class=\"day " . $morningClassName . " " . $eveningClassName . "\">" . $dayTide[dayIdx] . "</div>";
            	$mobileTable .= "\n\t<div class=\"xs-header\">" . $titles[dateIdx] . "</div><div class=\"" . $morningClassName . " " . $eveningClassName . "\">" . $dayTide[dateIdx] . "</div>";
            	if(strlen($morningClassName) > 0) {
		    	$mobileTable .= "\n\t<div class=\"xs-header\">" . $titles[morningHighTideIdx] . "</div><div class=\"" . $morningClassName . "\">" . $dayTide[morningHighTideIdx] . "</div>";
		    	$mobileTable .= "\n\t<div class=\"xs-header\">" . $titles[morningHighTideIdx+1] . "</div><div class=\"" . $morningClassName . "\">" . $dayTide[morningHighTideIdx+1] . "</div>";
		    	$mobileTable .= "\n\t<div class=\"xs-header\">" . $titles[morningHighTideCoeffIdx] . "</div><div class=\"" . $morningClassName . "\">" . $dayTide[morningHighTideCoeffIdx] . "</div>";
            	
            	}
            	if(strlen($eveningClassName) > 0) {
		    	$mobileTable .= "\n\t<div class=\"xs-header\">" . $titles[eveningHighTideIdx] . "</div><div class=\"" . $eveningClassName . "\">" . $dayTide[eveningHighTideIdx] . "</div>";
		    	$mobileTable .= "\n\t<div class=\"xs-header\">" . $titles[eveningHighTideIdx+1] . "</div><div class=\"" . $eveningClassName . "\">" . $dayTide[eveningHighTideIdx+1] . "</div>";
		    	$mobileTable .= "\n\t<div class=\"xs-header\">" . $titles[eveningHighTideCoeffIdx] . "</div><div class=\"" . $eveningClassName . "\">" . $dayTide[eveningHighTideCoeffIdx] . "</div>";
            	
            	}
            }
        }
        $table .= "\n\t</tbody>\n</table>";
        $mobileTable .= "\n</div>";
        fclose($monthTide);
        return $table . "\n\n\n" . $mobileTable;
    }
}
// echo buildMonthTable("ephemeride-avril.csv");
?> 
