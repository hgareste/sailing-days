
<?php
define("labels", ["", "janvier", "fevrier", "mars", "avril", "mai", "juin", "juillet", "aout", "septembre", "octobre", "novembre", "decembre"]);

function getMonthNameFromNumber($month) {
    // echo "$month : " . labels[intval($month)];
    return labels[intval($month)];
}

function parseLine($htmlLine) {

    // echo "$htmlLine\n";
    $nbFound = preg_match_all('|<time(.*)</time>|U', $htmlLine, $matches,  PREG_PATTERN_ORDER);
    if ($nbFound > 0){
        $listItems = $matches[1];

        // extract day info
        preg_match_all('|<span[^>]+>(.*)</span>|U', $listItems[0], $spanContentMatches,  PREG_PATTERN_ORDER);
        preg_match_all("/(\w+)/", $spanContentMatches[1][0], $dayMatches);
        $day = $dayMatches[0][0];
        // echo $day. "\n";

        // extract date info
        preg_match_all('|datetime="(.*)">|U', $listItems[0], $datetimeMatches,  PREG_PATTERN_ORDER);
        $date = $datetimeMatches[1][0];
        // echo "$date\n";

        // extract sunrise info
        $sunrise = explode(">", $listItems[1])[1];
        // echo "$sunrise\n";

        // extract sunset info
        $sunset = explode(">", $listItems[2])[1];
        // echo "$sunset\n";
        return "$day\t$date\t$sunrise\t$sunset";
    }
    // echo "empty line\n";
return "";
}

function loadSunDataFromSunriseSunset($year, $month) {

    $html = file_get_contents("https://www.sunrise-and-sunset.com/fr/sun/france/arcachon/$year/" . getMonthNameFromNumber($month));

    $start = stripos($html, '<tbody>');

    $end = stripos($html, '</tbody>', $offset = $start);

    $length = $end - $start;

    $htmlSection = substr($html, $start, $length);
    $htmlSection = str_replace("<tbody>", "", $htmlSection);
    $htmlSection = str_replace("</tbody>", "", $htmlSection);
    $htmlInlined = str_replace("\n", "", $htmlSection);
    $htmlInlined = preg_replace('/>[ \t]*</', "><", $htmlInlined);
    $htmlInlined = str_replace("<tr >", "", $htmlInlined);
    $htmlInlined = str_replace("</tr>", "\n", $htmlInlined);

    $htmlLines = explode("\n", $htmlInlined);

    $csvLines[] = "Jour\tDate\tLever du soleil\tCoucher du soleil ";

    // echo count($htmlLines) . "lines extracted\n";
    foreach ($htmlLines as $htmlLine) {
        // echo "$htmlLine\n";
        $csvData = parseLine($htmlLine);
        $csvLines[] = $csvData;
        // echo "$csvData\n";
        
    }

    file_put_contents("data/source-soleil-".$year."-".$month.".csv", implode("\n", $csvLines));
}

loadSunDataFromSunriseSunset("2023", "05");

// parseLine('<td class="alignmiddle"><a href="/fr/sun/france/arcachon/2023/janvier/29" title="Lever et coucher du soleil Arcachon, 29 janvier 2023"><time datetime="2023-01-29">29 janvier 2023<span class="hidden-sm hidden-xs underlined">, dimanche</span></time></a></td><td><time datetime="2023-01-29T08:28">08:28</time></td><td><time datetime="2023-01-29T18:07">18:07</time></td><td><time datetime="PT09H39M">09:39</time></td>');

?>
