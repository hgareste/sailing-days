
<?php

    require __DIR__ . '/build_ephemeride_table.php';
    require __DIR__ . '/load_tide_remote_data.php';
    require __DIR__ . '/load_sun_remote_data.php';
    require __DIR__ . '/merge_tide_sun.php';
    require __DIR__ . '/write_html_file.php';

    parse_str(implode('&', array_slice($argv, 1)), $_GET);

    $month = $_GET['month'];
    $year = $_GET['year'];


    $fileSuffix = "$year-$month.csv";

    $sourceTideFileName = "data/source-maree-$fileSuffix";
    $sourceSunFileName = "data/source-soleil-$fileSuffix";
    $ephemerideFileName = "data/ephemeride-$fileSuffix";
    $htmlFileName = "html/tides-$year-$month.html";

    if(! file_exists($sourceTideFileName)) {
        // echo "loading tide data";
        loadTideDataFromSiba($year, $month);
    }
    if(! file_exists($sourceSunFileName)) {
        // echo "loading sun data";
        loadSunDataFromSunriseSunset($year, $month);
    }
    if(! file_exists($ephemerideFileName)) {
        mergeTideAndSunMonthlyData($sourceTideFileName, $sourceSunFileName, $ephemerideFileName);
    }

    writeHtmlFile($ephemerideFileName, $htmlFileName);

?>
