<html>
    <head>
     <link rel="stylesheet" href="style.css"> 

    </head>
    <body>
    <?php

    require __DIR__ . '/build_ephemeride_table.php';
    require __DIR__ . '/load_sun_remote_data.php';
    require __DIR__ . '/load_tide_remote_data.php';
    require __DIR__ . '/merge_tide_sun.php';

    $month = $_GET['month'];
    $year = $_GET['year'];

/* protections pour ne pas saturer le répertoire avec des appels automatiques de robots */
if($year < 2023) {
    $year = 2023;
  }

  if($year > 2030) {
    $year = 2030;
  }

  function getPreviousMonthUrl($month, $year) {
    $previous = str_pad(intval($month, 10)-1, 2, '0', STR_PAD_LEFT);

    if($previous == 0) {
        $previous = "12";
        $year = str_pad(intval($year,10)-1, 2, '0', STR_PAD_LEFT);
    }

    return "https://latilloleboienne.fr/wordpress/sailing-calendar/sailing-days.php?month=" . $previous . "&year=" . $year;
}

function getNextMonthUrl($month, $year) {
    $next = str_pad(intval($month, 10)+1, 2, '0', STR_PAD_LEFT);

    if($next == "13") {
        $next = "01";
        $year = str_pad(intval($year,10)+1, 2, '0', STR_PAD_LEFT);
    }

    return "https://latilloleboienne.fr/wordpress/sailing-calendar/sailing-days.php?month=" . $next . "&year=" . $year;
}


    $fileSuffix = "$year-$month.csv";

    $sourceTideFileName = "data/source-maree-$fileSuffix";
    $sourceSunFileName = "data/source-soleil-$fileSuffix";
    $ephemerideFileName = "data/ephemeride-$fileSuffix";
    $htmlFileName = "html/tides-$year-$month.html";

    if(! file_exists($sourceTideFileName)) {
        // echo "loading tide data";
        loadTideDataFromSiba($year, $month);
    }
    if(! file_exists($sourceSunFileName)) {
        // echo "loading sun data";
        loadSunDataFromSunriseSunset($year, $month);
    }
    if(! file_exists($ephemerideFileName)) {
        mergeTideAndSunMonthlyData($sourceTideFileName, $sourceSunFileName, $ephemerideFileName);
    }
    
    $previousMonth = getPreviousMonthUrl($month, $year);
    $nextMonth = getNextMonthUrl($month, $year);
?>

<div class="links">
    <a href="<?php echo $previousMonth; ?>">Mois précédent</a>
    <span><?php echo $month . " / " . $year; ?></span>
    <a href="<?php echo $nextMonth; ?>">Mois suivant</a>
</div>

<?php
    echo buildMonthTable($ephemerideFileName);
    ?>
    </body>
</html>
